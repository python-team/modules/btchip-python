btchip-python (0.1.32-4) unstable; urgency=medium

  * Team upload.

  * Added AppStream metainfo XML with hardware provide
    info (Closes: #1076443).
  * Adjusted udev rules to grant device access to console users, not
    plugdev members (Closes: #1076442).

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 08 Jan 2025 00:40:28 +0100

btchip-python (0.1.32-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to move udev rules into /usr.

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 26 May 2024 23:49:04 +0200

btchip-python (0.1.32-3) unstable; urgency=medium

  * Team upload
  * Remove retired uploader
  * Patch: Remove invalid version in extras_require

 -- Bastian Germann <bage@debian.org>  Thu, 13 Jul 2023 13:48:30 +0200

btchip-python (0.1.32-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + python3-btchip: Drop versioned constraint on python3-usb in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 16 Nov 2022 10:34:14 +0000

btchip-python (0.1.32-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

  [ Debian Janitor ]
  * debian/control: Bump Standards-Version to 4.6.1.

 -- Boyuan Yang <byang@debian.org>  Sat, 08 Oct 2022 16:30:32 -0400

btchip-python (0.1.31-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Add dependency on python3-ecdsa (Closes: #981374)

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 08 Apr 2021 21:30:39 +0200

btchip-python (0.1.31-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Tristan Seligmann ]
  * New upstream release.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Tristan Seligmann <mithrandi@debian.org>  Thu, 05 Nov 2020 10:30:46 +0200

btchip-python (0.1.30-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper-compat.
  * Switch to dh-sequence-*.
  * Set Maintainer to DPMT.
  * Bump Standards-Version to 4.5.0 (no changes).
  * Declare rootless build.

 -- Tristan Seligmann <mithrandi@debian.org>  Fri, 24 Jul 2020 19:44:32 +0200

btchip-python (0.1.24-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #936246

 -- Sandro Tosi <morph@debian.org>  Thu, 17 Oct 2019 19:21:52 -0400

btchip-python (0.1.24-1) unstable; urgency=medium

  * New upstream release.
  * Move to Salsa and set Vcs-*.
  * Bump Standards-Version to 4.1.3 (no changes).
  * Build Python 3 package.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 07 Jan 2018 09:11:49 +0200

btchip-python (0.1.18-1) unstable; urgency=medium

  * New upstream version without changelog
  * Added nano s to the udev rule

 -- Richard Ulrich <richi@paraeasy.ch>  Wed, 31 Aug 2016 21:28:08 +0200

btchip-python (0.1.16-2) unstable; urgency=medium

  * changed dependency from python-hidapi to python-hid to match trezor and keepkey packages

 -- Richard Ulrich <richi@paraeasy.ch>  Wed, 18 May 2016 23:54:28 +0200

btchip-python (0.1.16-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * Initial release. (Closes: #762875)

 -- Richard Ulrich <richi@paraeasy.ch>  Mon, 22 Sep 2014 20:04:07 +0200
